package professional.hw4.task5;

import java.util.List;
import java.util.stream.Collectors;

public class Main {
    public static void main(String[] args) {
        List<String> list = List.of("abc", "def", "qqq");
        List<String> listUpperCase = list.stream()
                .map(String::toUpperCase)
                .collect(Collectors.toList());
        System.out.println(listUpperCase.stream()
                .collect(Collectors.joining(", ")));
    }
}
