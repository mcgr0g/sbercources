package professional.hw4.task3;

import java.util.List;

public class Main {
    public static void main(String[] args) {
        List<String> list = List.of("abc", "", "", "def", "qqq");
        long result = list.stream()
                .filter(e -> e.length() > 0)
                .count();
        System.out.println(result);
    }
}
