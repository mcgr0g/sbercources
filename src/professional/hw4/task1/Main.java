package professional.hw4.task1;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

import static java.util.stream.Collectors.toList;

public class Main {
    public static void main(String[] args) {
        int endNum = 100;
        List<Integer> numberArray = IntStream.range(1, endNum+1).boxed().collect(toList());
//        System.out.println(numberArray);

        int result = numberArray.stream()
                .filter(e -> e % 2 == 0)
                .reduce(0, Integer::sum);
        System.out.println(result);
    }
}
