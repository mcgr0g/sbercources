package professional.hw4.task6;

import java.util.Set;
import java.util.stream.Collectors;

public class Main {
    public static void main(String[] args) {
        Set<Set<Integer>> setSet = Set.of(Set.of(1, 2, 3), Set.of(3, 4, 5));
        System.out.println(setSet);

        Set<Integer> set = setSet.stream()
                .flatMap(innerSet -> innerSet.stream())
                .collect(Collectors.toSet());
        System.out.println(set);
    }
}
