package professional.hw4.task2;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        List<Integer> list = new ArrayList<>();

        Scanner scanner = new Scanner("1 2 3 4 5\n"); // true
//        Scanner scanner = new Scanner(System.in);
//        System.out.println("(EOF or non-integer to terminate): ");
        while (scanner.hasNextInt()){
            list.add(scanner.nextInt());
        }

        int result = list.stream()
                        .reduce(1, (x, y) -> x*y);
        System.out.println(result);
    }
}
