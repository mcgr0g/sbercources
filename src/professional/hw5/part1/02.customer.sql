create table customer (
    id serial primary key,
    name varchar(100) not null,
    cell_phone varchar(15) not null
);
commit;