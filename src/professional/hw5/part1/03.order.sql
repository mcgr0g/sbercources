create table orders (
    id serial primary key,
    customer_id int references customer (id),
    product_id int references product_price (id),
    count int not null check (count >= 1), check ( count <= 1000 ),
    created_at date
);
commit;

select * from orders;
