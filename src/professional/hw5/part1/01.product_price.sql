create table product_price (
    id serial primary key,
    name varchar(100) not null,
    price int not null
);
commit;