select pp.name, max(o.count) "cnt"
from orders o
join product_price pp on pp.id = o.product_id
group by 1
order by "cnt" desc
limit 1
;