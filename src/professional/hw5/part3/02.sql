select *
from orders o
join customer c on o.customer_id = c.id
where c.id = 1
and o.created_at >= CURRENT_DATE - interval '1' month
;