insert into orders (customer_id, product_id, count, created_at) values
(1, 1, 1000, now()),
(2, 2, 2, now() - interval '5d' ),
(3, 3, 1, now() - interval '45d');
commit ;