package professional.hw2.task05;

import java.util.*;

public class Main {
    public static void main(String[] args) {
        List<String> wordsForHistogram = new ArrayList<>(Arrays
                .asList("the", "day", "is", "sunny", "the", "the", "the", "sunny", "is", "is", "day"));
        int cutoff = 4;
        System.out.println(frequencyFilteredWords(wordsForHistogram, cutoff));
    }

    private static List<String> frequencyFilteredWords(List<String> words, int cutoff) {
        Map<String, Integer> wordFreqSet = new HashMap<>();
        // найдем частотность у каждого слова
        for (String s : words) {
            int counter = 1;
            if (wordFreqSet.containsKey(s)) {
                counter = wordFreqSet.get(s) + 1;
            }
            wordFreqSet.put(s, counter);
        }

        TreeSet<Integer> freqSet = new TreeSet<>(wordFreqSet.values());
        Set<Integer> freqSetReverse = freqSet.descendingSet();
        List<String> resultArr = new ArrayList<>();
        int resultArrayElemCounter = 0;
        for (Integer freqValue : freqSetReverse) {

            //для сортировки по лексикографии
            Set<String> tempSet = new HashSet<>();

            for (Map.Entry<String, Integer> wordFreq : wordFreqSet.entrySet()) {
                // можно было бы перейти на итератор, что бы удалять найденные пары, но не сложилось
                if (wordFreq.getValue().equals(freqValue)) {
                    tempSet.add(wordFreq.getKey());
                }
            }
            List<String> tempArr = new ArrayList<>(tempSet);
            //лексикография с убыванием в рамках одного значения частоты
            tempArr.sort(Collections.reverseOrder());
//            System.out.println(tempArr);

            for (String elem : tempArr) {
                if (resultArrayElemCounter < cutoff) {
                    resultArr.add(elem);
                    resultArrayElemCounter++;
                }
            }
        }



        return resultArr;
    }
}
