package professional.hw2.task03;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class Main {
    public static void main(String[] args) {
        Set<Integer> set1 = new HashSet<>(Arrays.asList(1,2,3));
        Set<Integer> set2 = new HashSet<>(Arrays.asList(0,1,2,4));
        System.out.println(set1);
        System.out.println(set2);

        System.out.println(PowerfulSet.intersection(set1, set2));
        System.out.println("----");
        System.out.println(set1);
        System.out.println(set2);
        System.out.println("----");

        System.out.println(PowerfulSet.union(set1, set2));
        System.out.println("----");
        System.out.println(set1);
        System.out.println(set2);
        System.out.println("----");

        System.out.println(PowerfulSet.relativeComplement(set1, set2));
    }
}
