package professional.hw2.task03;

import java.util.HashSet;
import java.util.Set;

public class PowerfulSet {
    private PowerfulSet() {
    }

    /*
     * возвращает пересечение двух наборов
     * Пример: set1 = {1, 2, 3}, set2 = {0, 1, 2, 4}. Вернуть {1, 2}
     * */
    public static <T> Set<T> intersection(Set<T> set1, Set<T> set2) {
        Set<T> toReturn = new HashSet<>();
        toReturn.addAll(set1);
        toReturn.retainAll(set2);
        return toReturn;
    }

    /*
     * возвращает объединение двух наборов.
     * Пример: set1 = {1, 2, 3}, set2 = {0, 1, 2, 4}. Вернуть {0, 1, 2, 3, 4}
     * */
    public static <T> Set<T> union(Set<T> set1, Set<T> set2) {
        Set<T> toReturn = new HashSet<>();
        toReturn.addAll(set1);
        toReturn.addAll(set2);
        return toReturn;
    }

    /*
     * возвращает элементы первого набора без тех,
     * которые находятся также и во втором наборе.
     * Пример: set1 = {1, 2, 3}, set2 = {0, 1, 2, 4}. Вернуть {3}
     * */
    public static <T> Set<T> relativeComplement(Set<T> set1, Set<T> set2) {
        Set<T> toReturn = new HashSet<>();
        toReturn.addAll(set1);
        toReturn.removeAll(set2);
        return toReturn;
    }
}
