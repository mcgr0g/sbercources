package professional.hw2.task02;


import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

/*
* С консоли на вход подается две строки s и t. Необходимо вывести true, если
одна строка является валидной анаграммой другой строки и false иначе.
Анаграмма — это слово или фраза, образованная путем перестановки букв
другого слова или фразы, обычно с использованием всех исходных букв ровно
один раз.
* */
public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String s = scanner.nextLine();
//        String s = "апельсин";
        String t = scanner.nextLine();
//        String t = "спаниель";

//        System.out.println(s);
//        System.out.println(t);
        System.out.println(isAnagrams(s, t));
    }

    public static boolean isAnagrams(String first, String second) {
        if (first.length() != second.length())
            return false;

        Map<Character, Integer> firstSet = new HashMap<>();
        for (char c: first.toCharArray()){
            int counter = 1;
            if (firstSet.containsKey(c)) {
                counter = firstSet.get(c) + 1;
            }
            firstSet.put(c, counter);
        }

        Map<Character, Integer> secondSet = new HashMap<>();
        for (char c: second.toCharArray()){
            int counter = 1;
            if (secondSet.containsKey(c)) {
                counter = secondSet.get(c) + 1;
            }
            secondSet.put(c, counter);
        }

        for (Map.Entry<Character, Integer> entry: firstSet.entrySet()){
            if (!secondSet.containsKey(entry.getKey()))
                return false;
            else if (!secondSet.get(entry.getKey()).equals(entry.getValue()))
                return false;
        }

        return true;
    }
}
