package professional.hw2.task01;

import java.util.ArrayList;
import java.util.Set;

public class Main {
    public static void main(String[] args) {
        ArrayList<String> rawData = new ArrayList<>();
        rawData.add("Новосибирск");
        rawData.add("Кемерово");
        rawData.add("Красноярск");
        rawData.add("Горноалтайск");
        rawData.add("Бишкек");
        rawData.add("Горноалтайск");
        rawData.add("Майма");

        System.out.println(rawData);
        Set<String> clearData = UniqueElementsOfList.getUniqueElements(rawData);
        System.out.println(clearData);
    }
}
