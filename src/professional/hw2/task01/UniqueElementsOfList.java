package professional.hw2.task01;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class UniqueElementsOfList {
    private UniqueElementsOfList(){}

    public static <T> Set<T> getUniqueElements(ArrayList<T> input){
        Set<T> resultSet = new HashSet<>();
        resultSet.addAll(input);
        return resultSet;
    }
}
