package professional.hw2.task04;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Document {
    public int id;
    public String name;
    public int pageCount;

    public Map<Integer, Document> organizeDocuments(List<Document> documents){
        Map<Integer, Document> mapDocs = new HashMap<>();
        for (Document doc: documents){
            mapDocs.put(doc.id, doc);
        }
        return mapDocs;
    }
}
