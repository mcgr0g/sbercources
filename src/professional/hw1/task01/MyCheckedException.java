package professional.hw1.task01;

/*
* Создать собственное исключение MyCheckedException, являющееся
проверяемым
* */
public class MyCheckedException extends Exception{
    public MyCheckedException(){}

    public MyCheckedException(String message){
        super(message);
    }
}
