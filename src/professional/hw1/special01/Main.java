package professional.hw1.special01;

import java.util.Arrays;
import java.util.Scanner;

/**
 На вход подается число n и массив целых чисел длины n.
 Вывести два максимальных числа в этой последовательности.
 Пояснение: Вторым максимальным числом считается тот, который окажется
 максимальным после вычеркивания первого максимума.
 */
public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int lengthOfArray = scanner.nextInt();

        int[] inputArray = new int[lengthOfArray];
        for (int i=0; i < lengthOfArray; i++)
            inputArray[i] = scanner.nextInt();

        int[] resultArray = new int[2];

        Arrays.sort(inputArray);

        resultArray[0] = inputArray[lengthOfArray-1];
        resultArray[1] = inputArray[lengthOfArray-2];

//        System.out.println(Arrays.toString(inputArray));
        System.out.println(Arrays.toString(resultArray));
    }
}
