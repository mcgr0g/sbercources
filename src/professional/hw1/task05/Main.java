package professional.hw1.task05;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        try {
            int n = inputN();
            System.out.println("Успешный ввод!");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static int inputN() throws Exception {
        System.out.println("Введите число n, 0 < n < 100");
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        if (n < 100 && n > 0) {
            return n;
        }
        else
            throw new Exception("Неверный ввод");
    }
}
