package professional.hw1.task06;

import java.time.LocalDate;

/*
Фронт со своей стороны не сделал обработку входных данных анкеты! Петя
очень зол и ему придется написать свои проверки, а также кидать исключения,
если проверка провалилась. Помогите Пете написать класс FormValidator со
статическими методами проверки. На вход всем методам подается String str
* */
public class FormValidator {

    /*
     * длина имени должна быть от 2 до 20
     * символов, первая буква заглавная
     * */
    public static void checkName(String str) throws RuntimeException {
        if (str.length() < 2 || str.length() > 20)
            throw new RuntimeException("некорректная длинна");
        if (Character.isLowerCase(str.charAt(0)))
            throw new RuntimeException("некорректный регистр первого символа");
    }

    /*
     * дата рождения должна быть не
     * раньше 01.01.1900 и не позже текущей даты
     * */
    public static void checkBirthdate(String str) throws RuntimeException {
        LocalDate date = LocalDate.parse(str);
        LocalDate rigthBorder = LocalDate.now();
        LocalDate leftBorder = LocalDate.of(1970, 1, 1);
        if (date.isBefore(leftBorder) || date.isEqual(rigthBorder)) {
            throw new RuntimeException("некорректная дата");
        }
    }

    /*
     * пол должен корректно матчится в
     * enum Gender, хранящий Male и Female значения
     * */
    public static void checkGender(String str) throws RuntimeException {
        if (!Gender.contains(str)) {
            throw new RuntimeException("пол не из справочника");
        }
    }

    /*
     * рост должен быть положительным
     * числом и корректно конвертироваться в double
     * */
    public static void checkHeight(String str) throws RuntimeException {
        try {
            double height = Double.parseDouble(str);
            if (height < 0)
                throw new RuntimeException("рост должен быть больше нуля");
        }
        catch (NumberFormatException ex) {
            throw new RuntimeException(ex);
        }
    }
}
