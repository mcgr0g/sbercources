package professional.hw1.task06;

public enum Gender {
    Male,
    Female;

    public String toString(){
        switch (this){
            case Male -> {
                return "Male";
            }
            case Female -> {
                return "Female";
            }
            default -> {
                return null;
            }
        }
    }
    
    public static boolean contains(String str){
        for (Gender gender : Gender.values()) {
            if (gender.name().equals(str))
                return true;
        }
        return false;
    }
}
