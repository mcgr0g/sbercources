package professional.hw1.task03;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.Scanner;

/*
 * Реализовать метод, открывающий файл ./input.txt и сохраняющий в файл
 * ./output.txt текст из input, где каждый латинский строчный символ заменен на
 * соответствующий заглавный. Обязательно использование try с ресурсами.
 * */
public class LatinUpperCaseRewriter {
    private static final String FILE_LOCATION = "src/professional.hw4/task03/";
    private static final String INPUT_FILE_NAME = "input.txt";
    private static final String OUTPUT_FILE_NAME = "output.txt";

    public static void main(String[] args) {
        try {
            readAndRewrite();
        }
        catch (IOException e){
            System.out.println("FileReadWrite#main!error: " + e.getMessage());
        }
    }

    public static void readAndRewrite() throws IOException {
        Scanner scanner = new Scanner(new File(FILE_LOCATION + INPUT_FILE_NAME));
        Writer writer = new FileWriter(FILE_LOCATION + OUTPUT_FILE_NAME);

        try (scanner; writer) {
            while (scanner.hasNext()) {
                writer.write(scanner.nextLine().toUpperCase() + "\n");
            }
        }
    }
}
