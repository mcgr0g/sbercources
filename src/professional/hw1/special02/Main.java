package professional.hw1.special02;

import java.util.Scanner;

/**
 * На вход подается число n, массив целых чисел отсортированных по
 * возрастанию длины n и число p. Необходимо найти индекс элемента массива
 * равного p. Все числа в массиве уникальны. Если искомый элемент не найден,
 * вывести -1.
 * Решить задачу за логарифмическую сложность.
 */
public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int lengthOfArray = scanner.nextInt();

        int[] inputArray = new int[lengthOfArray];
        for (int i = 0; i < lengthOfArray; i++)
            inputArray[i] = scanner.nextInt();

        int valueToSearch = scanner.nextInt();

//        System.out.println(Arrays.toString(inputArray));
        //бинарный поиск подходит по логарифмической сложности
        int leftIndex = 0;
        int rightIndex = lengthOfArray - 1;
        int keyToSearch = -1;

        while (leftIndex <= rightIndex) {
            int midIndex = leftIndex + (rightIndex - leftIndex)/2;
            int midValue = inputArray[midIndex];

            if (midValue < valueToSearch)
                leftIndex = midIndex + 1;
            else if (midValue > valueToSearch)
                rightIndex = midIndex -1;
            else {
                keyToSearch = midIndex;
                break;
            }
        }

        System.out.println(keyToSearch);
    }
}