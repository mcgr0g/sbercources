package professional.hw1.task02;

/*
* Создать собственное исключение MyUncheckedException, являющееся
непроверяемым.
* */
public class MyUncheckedException extends RuntimeException{
    public MyUncheckedException(){}

    public MyUncheckedException(String message){
        super(message);
    }
}
