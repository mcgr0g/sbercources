package professional.hw1.task04;

public class MyEvenNumber {
    private int n;

    public MyEvenNumber(int n) throws NonEvenException{
        if (n % 2 == 0){
            this.n = n;
        }
        else
            throw new NonEvenException(n);
    }

    public int getN(){
        return n;
    }
}
