package professional.hw1.task04;

public class NonEvenException extends Exception{
    private int number;

    public NonEvenException(int number){
        super("Число " + number + " - нечетное");
        this.number = number;
    }

    public int getNumber(){
        return number;
    }
}
