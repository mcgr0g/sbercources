package professional.hw3.task4;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/*
 * Написать метод, который с помощью рефлексии получит все интерфейсы
 * класса, включая интерфейсы от классов-родителей и интерфейсов-родителей.
 * */
public class FamilyClassExplorer {
    public static void main(String[] args) {
        List<Class<?>> interfaces = getAllInterfaces(SonClassSab.class);
        for (Class<?> cls : interfaces)
            System.out.println(cls.getName());
    }

    public static List<Class<?>> getAllInterfaces(Class<?> cls) {
        List<Class<?>> interfaces = new ArrayList<>();

        while (cls != Object.class){
            List<Class<?>> foundInterfacesInIteration = new ArrayList<>(Arrays.asList(cls.getInterfaces()));
            for (Class<?> iface : foundInterfacesInIteration){
                interfaces.addAll(Arrays.asList(iface.getInterfaces()));
            }
            for (Class<?> iface : foundInterfacesInIteration){
                if (!interfaces.contains(iface))
                    interfaces.add(iface);
            }
            cls = cls.getSuperclass();
        }

        return interfaces;
    }
}
