package professional.hw3.task6s;

import java.util.Scanner;
import java.util.Stack;

/*
 * Дана строка, состоящая из символов “(“, “)”, “{”, “}”, “[“, “]”
 * Необходимо написать метод, принимающий эту строку и выводящий результат,
 * является ли она правильной скобочной последовательностью или нет.
 * */
public class Main {
    public static void main(String[] args) {
//        Scanner scanner = new Scanner(System.in);
        Scanner scanner = new Scanner("{()[]()}"); // true
//        Scanner scanner = new Scanner("{)(}"); // false
//        Scanner scanner = new Scanner("[}"); // false
//        Scanner scanner = new Scanner("[{(){}}][()]{}"); // true
//        Scanner scanner = new Scanner("[(]{})"); // false
        String input = scanner.nextLine();


        System.out.println(isBracketSeqIsCorrect(input));
    }

    public static boolean isBracketSeqIsCorrect(String input) {

        boolean smthBad = false;
        Stack<Character> bracketStack = new Stack<>();

        for (int i = 0; i < input.length(); i++) {

            char bracketChar = input.charAt(i);

            switch (bracketChar) {
                case '{' -> bracketStack.push(bracketChar);
                case '}' -> smthBad = !isCharExtractedAndEquals('{', bracketStack);
                case '[' -> bracketStack.push(bracketChar);
                case ']' -> smthBad = !isCharExtractedAndEquals('[', bracketStack);
                case '(' -> bracketStack.push(bracketChar);
                case ')' -> smthBad = !isCharExtractedAndEquals('(', bracketStack);
                default -> {
                    return false;
                }
            }
            if (smthBad)
                return false;

        }

        return bracketStack.isEmpty();
    }

    public static boolean isCharExtractedAndEquals(char c, Stack<Character> stack) {
        if (stack.isEmpty())
            return false;
        return c == stack.pop();
    }
}
