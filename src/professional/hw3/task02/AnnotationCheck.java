package professional.hw3.task02;

import professional.hw3.task01.IsLike;

public class AnnotationCheck {
    public static void main(String[] args) {
        System.out.println(isSettedIsLike(Main.class));
    }

    private static boolean isSettedIsLike(Class<?> cls) {
        if (!cls.isAnnotationPresent(IsLike.class)) {
            return false;
        }
        IsLike isLike = cls.getAnnotation(IsLike.class);
        System.out.println("value: " + isLike.value());
        return true;
    }
}
