package professional.hw3.task3;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/*
Задача: с помощью рефлексии вызвать метод print() и обработать все
возможные ошибки (в качестве аргумента передавать любое подходящее
число). При “ловле” исключений выводить на экран краткое описание ошибки.
* */
public class ReflectionMethodTest {

    public static void main(String[] args) {
        Class<APrinter> aPrinterClass = APrinter.class;

        try {
            Object aPrinterObj = aPrinterClass.newInstance();
            Method method = aPrinterClass.getMethod("print", int.class);
            method.invoke(aPrinterObj, 3);
        } catch (InstantiationException
                | InvocationTargetException
                | NoSuchMethodException
                | IllegalAccessException
                | IllegalArgumentException e) {
            System.out.println("ERROR: " + e.getMessage());
        }
    }
}
