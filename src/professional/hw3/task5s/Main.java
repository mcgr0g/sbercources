package professional.hw3.task5s;

import java.util.Scanner;


/*
 * Необходимо написать метод, принимающий строку и выводящий результат,
 * является ли она правильной скобочной последовательностью или нет.
 * */
public class Main {

    public static final Character leftBorder = '(';
    public static final Character rightBorder = ')';

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
//        Scanner scanner = new Scanner("((()))");
        String input = scanner.nextLine();

        System.out.println(isBracketSeqIsCorrect(input));
    }

    private static boolean isBracketSeqIsCorrect(String input) {
        boolean smthBad = false;
        int openBracketCounter = 0;

        for (int i = 0; i < input.length(); i++) {
            if (input.charAt(i) == leftBorder) {
                openBracketCounter++;
            } else if (input.charAt(i) == rightBorder) {
                openBracketCounter--;
            } else {
                smthBad = true;
            }
            if (openBracketCounter < 0) {
                smthBad = true;
            }
        }

        if (smthBad || openBracketCounter > 0)
            return false;
        else
            return true;
    }
}
