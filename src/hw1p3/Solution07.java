package hw1p3;

import java.sql.RowId;
import java.util.Scanner;
/*
Дана строка s. Вычислить количество символов в ней, не считая пробелов
(необходимо использовать цикл).
*/
public class Solution07 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String s = scanner.nextLine();
        scanner.close();
        
        int result = 0;
        for (int i=0; i < s.length(); i++){
            if (s.charAt(i) != ' ')
                result++;
        }
        System.out.println(result);
    }
}