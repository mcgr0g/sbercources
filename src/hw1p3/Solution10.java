package hw1p3;

import java.util.Scanner;
/*
Вывести на экран "ёлочку" из символа решетки (#) заданной высоты N.
На N + 1 строке у "ёлочки" должен быть отображен ствол из символа |

Ограничение:
2 < n < 10
*/
public class Solution10 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        scanner.close();

        for (int level = 1; level <= n; level++){
            printLayer(level, n);
        }
        for (int i = 1; i <=n; i ++){
            if (i < n)
                System.out.print(" ");
            else
                System.out.print("|");
        }
    }

    public static void printLayer(int currentLevel, int maxLevel){
        for (int i = 1; i <= maxLevel - currentLevel; i++){
            System.out.print(" ");
        }
        for (int i = 1; i <= (currentLevel -1)*2 + 1; i++){
            System.out.print("#");
        }
        System.out.println();
    }
}