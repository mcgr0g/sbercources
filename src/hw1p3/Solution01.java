package hw1p3;

public class Solution01 {
    public static void main(String[] args) {
        for (int left = 1; left <= 9; left ++){
            for (int right = 1; right <= 9; right++){
                System.out.println(left + " x " + right + " = " + left*right);
            }
            System.out.println("--------");
        }
    }
}
