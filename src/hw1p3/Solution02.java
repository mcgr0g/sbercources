package hw1p3;

import java.util.Scanner;
/*
 На вход подается два положительных числа m и n. Найти сумму чисел между m
и n включительно.
Ограничения:
0 < m, n < 10
m < n
*/
public class Solution02{
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int m = scanner.nextInt();
        int n = scanner.nextInt();
        scanner.close();

        if (m > n){
            int tmp = n;
            n = m;
            m=tmp;
        }
        //теперь все согласно условию 0 < m < n < 10
        int res=0;
        for(int i = m; i <= n;i++)
            res += i;
        System.out.println(res);
    }
}