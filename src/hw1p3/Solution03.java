package hw1p3;

import java.util.Scanner;
/*
На вход подается два положительных числа m и n. Необходимо вычислить m^1
+ m^2 + ... + m^n
Ограничения:
0 < m, n < 10
*/
public class Solution03 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int m = scanner.nextInt();
        int n = scanner.nextInt();
        scanner.close();

        // if (m > n){
        //     int tmp = n;
        //     n = m;
        //     m=tmp;
        // }
        //теперь все согласно условию 0 < m < n < 10

        int res=0;
        for(int i = 1; i <= n;i++)
            res += Math.pow(m, i);
        System.out.println(res);
    }
}