package hw1p3;

import java.util.Scanner;
/*
Дано натуральное число n. Вывести его цифры в “столбик”.
Ограничения:
0 < n < 1000000
*/
public class Solution04 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        scanner.close();

        String digits = String.valueOf(n);
        for (int i = 0; i < digits.length(); i++){
            System.out.println(digits.charAt(i));
        }
    }
}