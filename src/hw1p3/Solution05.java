package hw1p3;

import java.util.Scanner;
/*
Даны положительные натуральные числа m и n. Найти остаток от деления m на
n, не выполняя операцию взятия остатка.
Ограничения:
0 < m, n < 10
*/
public class Solution05 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int m = scanner.nextInt();
        int n = scanner.nextInt();
        scanner.close();

        int temp = m/n;
        System.out.println(m - n*temp);
    }
}