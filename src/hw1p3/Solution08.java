package hw1p3;

import java.util.Scanner;
/*
На вход подается:
- целое число n
- целое число p
- целые числа a1, a2 , … an

Необходимо вычислить сумму всех чисел a1, a2, a3 … an которые строго больше p.

Ограничение:
0 < m, n, ai < 1000
*/
public class Solution08 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int p = scanner.nextInt();
        
        int result = 0;
        for (int i = 0; i < n; i ++) {
            int a = scanner.nextInt();
            if (a > p)
                result += a;
        }
        scanner.close();
        System.out.println(result);
    }
}