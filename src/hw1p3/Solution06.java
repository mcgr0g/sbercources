package hw1p3;

import java.sql.RowId;
import java.util.Scanner;
/*
Даны положительные натуральные числа m и n. Найти остаток от деления m на
n, не выполняя операцию взятия остатка.
Ограничения:
0 < m, n < 10
*/
public class Solution06 {
    public static final int NOTE_8 = 8;
    public static final int NOTE_4 = 4;
    public static final int NOTE_2 = 2;
    public static final int NOTE_1 = 1;

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        scanner.close();

        int amountForNote;// целое количество купюр
        int remain; // остаток, который нужно поделить между купюрами
        
        amountForNote = n / 8;
        remain = n - amountForNote*8;
        System.out.print(amountForNote + " ");
        n = remain;

        amountForNote = n / 4;
        remain = n - amountForNote*4;
        System.out.print(amountForNote + " ");
        n = remain;

        amountForNote = n / 2;
        remain = n - amountForNote*2;
        System.out.print(amountForNote + " ");
        n = remain;

        amountForNote = n / 1;
        remain = n - amountForNote;
        System.out.print(amountForNote);
    }
}