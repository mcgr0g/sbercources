package hw2p1;

import java.util.Scanner;
import java.util.Arrays;
import java.util.Locale;
/*
На вход подается число N — длина массива. Затем передается массив
целых чисел (ai) из N элементов. После этого передается число M — величина
сдвига.
Необходимо циклически сдвинуть элементы массива на M элементов вправо.
*/
public class Solution05 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);
        int n = scanner.nextInt();
        int[] arr1 = new int[n];
        for (int i=0; i < n; i++)
            arr1[i] = scanner.nextInt();
        int m = scanner.nextInt();
        scanner.close();

        int[] arrTmp = new int[m];
        //копируем в буфер хвост, который должен вылезти за границу массива
        System.arraycopy(arr1, n-m, arrTmp, 0, m);
        //свигаем на m элементы до правой границы. Сдвигает m+1 количество элементов,
        // потому что нужно захватить нулевой элемент
        System.arraycopy(arr1, 0, arr1, m, m+1);
        //вставляем буфер в начало
        System.arraycopy(arrTmp, 0, arr1, 0, m);

        for (int elem: arr1)
            System.out.print(elem + " ");
    }
}