package hw2p1;

import java.util.Scanner;

import java.util.Locale;
/*
На вход подается число N — длина массива. Затем передается массив
целых чисел (ai) из N элементов, отсортированный по возрастанию. После этого
вводится число X — элемент, который нужно добавить в массив, чтобы
сортировка в массиве сохранилась.
Необходимо вывести на экран индекс элемента массива, куда нужно добавить
X. Если в массиве уже есть число равное X, то X нужно поставить после уже
существующего
*/
public class Solution03 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);
        int n = scanner.nextInt();
        int[] arr1 = new int[n];
        for (int i=0; i < n; i++)
            arr1[i] = scanner.nextInt();
        int x = scanner.nextInt();
        scanner.close();

        int targetPosotion = 0;
        for (int i = 0; i < arr1.length; i++) {
            if (arr1[i] <= x){
                targetPosotion = i+1;
            } else
                break;
        }

        System.out.println(targetPosotion);
    }
}