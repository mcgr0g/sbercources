package hw2p1;

import java.util.Scanner;
import java.util.Arrays;
/*
На вход подается число N — длина массива. Затем передается массив
целых чисел (ai) из N элементов, отсортированный по возрастанию.
Необходимо создать массив, полученный из исходного возведением в квадрат
каждого элемента, упорядочить элементы по возрастанию и вывести их на
экран.
*/
public class Solution07 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[] arr1 = new int[n];
        for (int i=0; i < n; i++)
            arr1[i] = scanner.nextInt();
        scanner.close();

        for (int i = 0; i < arr1.length; i++) {
            arr1[i] = arr1[i] * arr1[i];
        }

        Arrays.sort(arr1);

        for (int i : arr1) {
            System.out.print(i + " ");
        }
   }
}