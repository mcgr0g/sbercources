package hw2p1;

import java.util.Arrays;
import java.util.Scanner;

public class Solution06 {
    public static void main(String[] args) {
         Scanner scanner = new Scanner(System.in);
         String s = scanner.nextLine();
         scanner.close();

        String[] morseArray = {".-", "-...", ".--", "--.", "-..", ".", "...-", "--..", "..", ".---", "-.-", ".-..", "--", "-.", "---", ".--.", ".-.",
                "...", "-", "..-", "..-.", "....", "-.-.", "---.", "----", "--.-", "--.--", "-.--", "-..-", "..-..", "..--", ".-.-"};

        Character[] russianArray = new Character[32];
        int tempCnt = 0;
        for (char symbol = 'А'; symbol <= 'Я'; symbol++){
            if(symbol != 'Ё'){
                russianArray[tempCnt] = symbol;
                tempCnt++;
            }
        }

        for (int i = 0; i < s.length(); i++) {
            Character c = Character.toUpperCase(s.charAt(i));
            int position = Arrays.binarySearch(russianArray, c);
            String morseLetter = morseArray[position];
            System.out.print(morseLetter + " ");
        }
    }
}
