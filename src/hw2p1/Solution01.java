package hw2p1;

import java.util.Scanner;
import java.util.Locale;
/*
На вход подается число N — длина массива. Затем передается массив
вещественных чисел (ai) из N элементов.
Необходимо реализовать метод, который принимает на вход полученный
массив и возвращает среднее арифметическое всех чисел массива.
Вывести среднее арифметическое на экран
*/
public class Solution01 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);
        int n = scanner.nextInt();
        double[] arr = new double[n];

        for (int i=0; i < n; i++)
            arr[i] = scanner.nextDouble();
    
        System.out.println(printArithmeticalMean(arr));
        scanner.close();
    }

    public static double printArithmeticalMean(double[] nums){
        double result = 0;
        for (double num: nums){
            result += num;
        }
        result = result / nums.length;
        return result;
    }
}