package hw2p1;

import java.util.Scanner;

import javax.lang.model.element.Element;

import java.util.Locale;
/*
На вход подается число N — длина массива. Затем передается массив
целых чисел (ai) из N элементов. После этого аналогично передается второй
массив (aj) длины M.
Необходимо вывести на экран true, если два массива одинаковы (то есть
содержат одинаковое количество элементов и для каждого i == j элемент ai ==
aj). Иначе вывести false.
*/
public class Solution02 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);
        int n = scanner.nextInt();
        int[] arr1 = new int[n];
        for (int i=0; i < n; i++)
            arr1[i] = scanner.nextInt();

        int m = scanner.nextInt();
        int[] arr2 = new int[m];
        for (int i=0; i < m; i++)
            arr2[i] = scanner.nextInt();

        scanner.close();

        boolean equalFlag = arr1.length == arr2.length;
        if (equalFlag){
            for (int i = 0; i < arr1.length; i++){
                if (arr1[i] != arr2[i]){
                    equalFlag = false;
                    break;
                }
            }
        }
        System.out.println(equalFlag);
    }
}