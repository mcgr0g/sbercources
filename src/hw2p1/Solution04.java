package hw2p1;

import java.util.Scanner;

import java.util.Locale;
/*
На вход подается число N — длина массива. Затем передается массив
целых чисел (ai) из N элементов, отсортированный по возрастанию.
Необходимо вывести на экран построчно сколько встретилось различных
элементов. Каждая строка должна содержать количество элементов и сам
элемент через пробел.
*/
public class Solution04 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);
        int n = scanner.nextInt();
        int[] arr1 = new int[n];
        for (int i=0; i < n; i++)
            arr1[i] = scanner.nextInt();
        scanner.close();

        int valueCounter=1;
        for (int i = 0; i < arr1.length; i++) {
            if ((i+1 < n) && (arr1[i] == arr1[i+1])){
                valueCounter++;
            } else {
                System.out.println(valueCounter + " " + arr1[i]);
                valueCounter = 1;
            }
        }
    }
}