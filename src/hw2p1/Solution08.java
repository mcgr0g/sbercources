package hw2p1;

import java.util.Scanner;
import java.util.Arrays;
/*
На вход подается число N — длина массива. Затем передается массив
целых чисел (ai) из N элементов. После этого передается число M.
Необходимо найти в массиве число, максимально близкое к M (т.е. такое число,
для которого |ai - M| минимальное). Если их несколько, то вывести
максимальное число.
*/
public class Solution08 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[] arr1 = new int[n];
        for (int i=0; i < n; i++)
            arr1[i] = scanner.nextInt();
        int m = scanner.nextInt();
        scanner.close();

        int positionFinal = 0;
        int distanceFinal = Math.abs(arr1[positionFinal] - m);
        for (int i = 1; i < arr1.length; i++) {
            int distance = Math.abs(arr1[i] - m);
            if (distance < distanceFinal){
                distanceFinal = distance;
                positionFinal = i;
            } else if (distance == distanceFinal){
                if (arr1[i] > arr1[positionFinal]){
                    positionFinal = i;
                }
            }
        }
        System.out.println(arr1[positionFinal]);
   }
}