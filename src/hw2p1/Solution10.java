package hw2p1;

import java.util.Scanner;

/*
Необходимо реализовать игру. Алгоритм игры должен быть записан в
отдельном методе. В методе main должен быть только вызов метода с
алгоритмом игры.
Условия следующие:
Компьютер «загадывает» (с помощью генератора случайных чисел) целое
число M в промежутке от 0 до 1000 включительно. Затем предлагает
пользователю угадать это число. Пользователь вводит число с клавиатуры.
Если пользователь угадал число M, то вывести на экран "Победа!". Если
введенное пользователем число меньше M, то вывести на экран "Это число
меньше загаданного." Если введенное число больше, то вывести "Это число
больше загаданного." Продолжать игру до тех пор, пока число не будет отгадано
или пока не будет введено любое отрицательное число.
*/
public class Solution10 {
    public static final String WIN_STRING = "Победа!";
    public static final String BELOW_STRING = "Это число меньше загаданного.";
    public static final String AFTER_STRING = "Это число больше загаданного.";

    public static void main(String[] args) {
        game();
    }

    public static void game() {
        Scanner scanner = new Scanner(System.in);

        int riddleNumber = (int) (Math.random() * 1001);
        boolean riddleFound = false;
        int m;
        System.out.println(riddleNumber);
        do {
            m = scanner.nextInt();
            if (m > riddleNumber)
                System.out.println(AFTER_STRING);
            else if (m < riddleNumber)
                System.out.println(BELOW_STRING);
            else {
                riddleFound = true;
                System.out.println(WIN_STRING);
            }
        } while (!riddleFound && m >= 0);

    }
}
