package hw3p2;

import java.util.UUID;

public class BookFeedback {
    private UUID visitorId;
    private int rank;

    public BookFeedback(UUID visitorId, int rank) {
        this.rank = rank;
        this.visitorId = visitorId;
    }

    public UUID getVisitor() {
        return visitorId;
    }

    public int getRank() {
        return rank;
    }

}
