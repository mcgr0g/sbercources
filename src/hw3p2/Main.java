package hw3p2;

public class Main {
    public static void main(String[] args) {
        Book book1 = new Book("Легкий способ сдать домашку", "Аллиен Карр");
        Book book2 = new Book("Работа с конкуренцией", "Ололоша Ололоев");
        Book book3 = new Book("Не изданное", "Ололоша Ололоев");
        Book book4 = new Book("Легкий способ запороть тест кейс", "Аллиен Карр");

        Visitor visitor1 = new Visitor("Предатор");
        Visitor visitor2 = new Visitor("Шварц");

        Library lib1 = new Library();
        lib1.add(book1);
        lib1.add(book2);
        lib1.add(book3);
        lib1.add(book4);

        System.out.println(lib1.toString(lib1.getBookList("Ололоша Ололоев")));
        System.out.println(lib1.removeAndReportSuccess("изданное"));
        System.out.println(lib1.removeAndReportSuccess("Не изданное"));
        System.out.println();

        System.out.println("кому выдана book1: " + book1.getBorrowedTo());
        System.out.println("успешность попытки выдать book1 посетителя visitor1: "
                + lib1.borrowBookAndReportSuccess(book1, visitor1));
        System.out.println("кому выдана book1: " + book1.getBorrowedTo());
        System.out.println();

        System.out.println("успешность попытки выдать book4 посетителя visitor1: "
                + lib1.borrowBookAndReportSuccess(book4, visitor1));
        System.out.println("visitor1.getBookCounter() = " + visitor1.getBookCounter());
        System.out.println();

        System.out.println("кому выдана book1: " + book1.getBorrowedTo());
        System.out.println("успешность попытки выдать book1 посетителю visitor2: "
                + lib1.borrowBookAndReportSuccess(book1, visitor2));

        System.out.println("успешность попытки вернуть book1 от посетителя visitor2: "
                + lib1.returnBoorAndReportSuccess(book1, visitor2));
        System.out.println();

        System.out.println("кто такой visitor1: " + visitor1.getId());
        System.out.println("успешность попытки вернуть book1 от посетителя visitor1: "
                + lib1.returnBoorAndReportSuccess(book1, visitor1));
        System.out.println();

        System.out.println("###### special part#####");

        System.out.println("успешность попытки выдать book4 посетителю visitor2: "
                + lib1.borrowBookAndReportSuccess(book4, visitor2));
        System.out.println("кто такой visitor2: " + visitor2.getId());
        System.out.println("успешность попытки вернуть book1 от посетителя visitor2: "
                + lib1.returnBoorAndReportSuccess(book4, visitor2, 4));
        System.out.println("оценка книги = " + book4.getAVGFeedback());

        System.out.println("успешность попытки выдать book4 посетителю visitor1: "
                + lib1.borrowBookAndReportSuccess(book4, visitor1));
        System.out.println("успешность попытки вернуть book4 от посетителю visitor1: "
                + lib1.returnBoorAndReportSuccess(book4, visitor1, 2));
        System.out.println("оценка книги = " + book4.getAVGFeedback());
    }
}
