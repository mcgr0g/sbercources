package hw3p2;

import java.util.UUID;

public class Visitor {
    private String name;
    private UUID id;
    private int bookCounter;
    public static final int MAX_BOOK_BOROWWED = 1;

    public Visitor(String name) {
        this.name = name;
        this.id = null;
        this.bookCounter = 0;
    }

    public String getName() {
        return name;
    }

    public UUID getId() {
        return id;
    }

    public void setId() {
        this.id = UUID.randomUUID();
    }

    public int getBookCounter() {
        return bookCounter;
    }

    public void setBookCounter(int bookCounter) {
        this.bookCounter = bookCounter;
    }
}
