package hw3p2;

import java.util.ArrayList;
import java.util.NoSuchElementException;

import static hw3p2.Visitor.MAX_BOOK_BOROWWED;

public class Library {
    private ArrayList<Book> bookList;

    public Library() {
        bookList = new ArrayList<>();
    }

    public boolean isBookRegistered(String bookName) {
        for (Book elem : bookList) {
            if (elem.getName().equals(bookName))
                return true;
        }
        return false;
    }

    public void add(Book book) {
        boolean registered = false;
        if (!isBookRegistered(book.getName()))
            bookList.add(book);
    }

    public Book findBookViaName(String bookName) throws NoSuchElementException {
        for (Book elem : bookList) {
            if (elem.getName().equals(bookName))
                return elem;
        }
        throw new NoSuchElementException("книга " + bookName + " не зарегистрирована");
    }

    public ArrayList<Book> getBookList(String author) throws NoSuchElementException {
        boolean isAuthorRegistered = false;
        ArrayList<Book> resultList = new ArrayList<>();
        for (Book elem : bookList) {
            if (elem.getAuthor().equals(author)) {
                isAuthorRegistered = true;
                resultList.add(elem);
            }
        }
        if (!isAuthorRegistered)
            throw new NoSuchElementException("автор " + author + " не зарегистрирован");
        return resultList;
    }

    public String toString(ArrayList<Book> books){
        StringBuilder str = new StringBuilder();
        for (Book book: books) {
            str.append(book.getAuthor()).append("\t");
            str.append(book.getName()).append("\t");
            str.append(book.getBorrowedTo());
        }

        return str.toString();
    }

    public boolean removeAndReportSuccess(String bookName) {
        try {
            Book suspect = findBookViaName(bookName);
            if (suspect.isBorrowed())
                return false;

            bookList.remove(suspect);
            return true;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return false;
        }
    }

    public boolean borrowBookAndReportSuccess(Book book, Visitor visitor) {
        if (visitor.getBookCounter() < MAX_BOOK_BOROWWED && isBookRegistered(book.getName())) {
            if (visitor.getId() == null)
                visitor.setId();
            if (book.markBorrowedAndReport(visitor.getId())) {
                visitor.setBookCounter(visitor.getBookCounter() + 1);
//                System.out.println("visitor " + visitor.getName() + " now has bookCounter  = " + visitor.getBookCounter());
                return true;
            }
            return false;
        }

        return false;
    }

    public boolean returnBoorAndReportSuccess(Book book, Visitor visitor) {
        if (book.isBorrowed() && book.getBorrowedTo().equals(visitor.getId()) ) {
            visitor.setBookCounter(visitor.getBookCounter() - 1);
            book.returnToLib();
            return true;
        }
        return false;
    }

    public boolean returnBoorAndReportSuccess(Book book, Visitor visitor, int feedbackVote) {
        if (returnBoorAndReportSuccess(book, visitor)) {
            book.appendFeedback(visitor, feedbackVote);
            return true;
        }
        return false;
    }
}
