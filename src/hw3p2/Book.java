package hw3p2;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class Book {
    private String name;
    private String author;
    private UUID borrowedTo;
    private List<BookFeedback> feedback;

    public Book(String name, String author) {
        this.name = name;
        this.author = author;
        this.borrowedTo = null;
        this.feedback = new ArrayList<>();
    }

    public String getName() {
        return name;
    }

    public String getAuthor() {
        return author;
    }

    public UUID getBorrowedTo() {
        return borrowedTo;
    }

    public boolean markBorrowedAndReport(UUID borrowedTo) {
        if (!isBorrowed()) {
            this.borrowedTo = borrowedTo;
            return true;
        }
        else
            return false;
    }

    public boolean isBorrowed() {
        return borrowedTo != null;
    }

    public void returnToLib(){
        this.borrowedTo = null;
    }

    public List<BookFeedback> getFeedbackList() {
        return feedback;
    }

    public void appendFeedback(Visitor visitor, int feedbackVote) {
        this.feedback.add(new BookFeedback(visitor.getId(), feedbackVote));
    }

    public double getAVGFeedback() {
        if (feedback == null || feedback.size() == 0 )
            return 0;
        double result = 0;
        for (BookFeedback elem: feedback) {
            result += elem.getRank();
        }
        return result / feedback.size();
    }
}
