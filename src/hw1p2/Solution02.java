package hw1p2;

import java.util.Scanner;

/*
2. Первый квадрант
Петя пришел домой и помогает дочке решать математику.
Ей нужно определить, принадлежит ли точка с указанными координатами первому квадранту.
Недолго думая, Петя решил автоматизировать процесс и написать программу: на вход нужно принимать два целых числа (координаты точки), выводить true, когда точка попала в квадрант и false иначе.
Но сначала Петя вспомнил, что точка лежит в первом квадранте тогда, когда её координаты удовлетворяют условию: x > 0 и y > 0.

Ограничение:
-100 < x, y < 100

Примечание:
Решение должно находиться в файле с названием Solution.java.
Публичный класс с решением должен называться Solution.
Использовать package нельзя.

Пример входных данных
-50 -12


Пример выходных данных
false
*/
public class Solution02 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int x = scanner.nextInt();
        int y = scanner.nextInt();
        scanner.close();

        String msgCool = "true";
        String msgFail = "false";

        if (x > 0 && y > 0){
            System.out.println(msgCool);
        } else{
            System.out.println(msgFail);
        }
    }
}
