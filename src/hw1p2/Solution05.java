package hw1p2;

import java.util.Scanner;

/*
5. Квадратичное уравнение
Дома дочери Пети опять нужна помощь с математикой!
В этот раз ей нужно проверить, имеет ли предложенное квадратное уравнение решение или нет.

На вход подаются три числа — коэффициенты квадратного уравнения a, b, c.
Нужно вывести "Решение есть", если оно есть и "Решения нет", если нет.

Ограничение:
-100 < a, b, c < 100

Примечание:
Решение должно находиться в файле с названием Solution.java.
Публичный класс с решением должен называться Solution.
Использовать package нельзя.

Пример входных данных
1 -95 18


Пример выходных данных
Решение есть
*/
public class Solution05 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int a = scanner.nextInt();
        int b = scanner.nextInt();
        int c = scanner.nextInt();
        scanner.close();

        String msgPositive = "Решение есть";
        String msgFail = "Решения нет";

        if (b*b - 4*a*c < 0){
            System.out.println(msgFail);
        } else{
            System.out.println(msgPositive);
        }
    }
}
