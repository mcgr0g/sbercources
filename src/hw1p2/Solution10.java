package hw1p2;

import java.util.Scanner;

/*
10. Логарифмическое тождество
"А логарифмическое?" - не унималась дочь.

Ограничение:
-500 < n < 500

Примечание:
Решение должно находиться в файле с названием Solution.java.
Публичный класс с решением должен называться Solution.
Использовать package нельзя.

Пример входных данных
1.0


Пример выходных данных
true
 */
public class Solution10 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        double input = scanner.nextDouble();
        scanner.close();

        if (Math.log(Math.exp(input)) == input)
            System.out.println("true");
        else
            System.out.println(false);
    }
}
