package hw1p2;

import java.util.Scanner;

/*
8. Разделение строки 2
Раз так легко получается разделять по первому пробелу, Петя решил немного изменить предыдущую программу и теперь разделять строку по последнему пробелу.

Ограничение:
В строке гарантированно есть хотя бы один пробел
Первый и последний символ строки гарантированно не пробел
2 < s.length() < 100

Примечание:
Решение должно находиться в файле с названием Solution.java.
Публичный класс с решением должен называться Solution.
Использовать package нельзя.

Пример входных данных
Hi great team!


Пример выходных данных
Hi great
team!
*/
public class Solution08 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String input = scanner.nextLine();
        scanner.close();

        int position = input.lastIndexOf(" ");
        String sub1 = input.substring(0, position);
        String sub2 = input.substring(position+1);
        
        System.out.println(sub1);
        System.out.println(sub2);
    }
}
