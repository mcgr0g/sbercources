package hw1p2;

import java.util.Scanner;

/*
9. Тригонометрическое тождество
Пока Петя практиковался в работе со строками, к нему подбежала его дочь и спросила: "А правда ли, что тригонометрическое тождество (sin^2(x)+ cos^2(x) - 1 == 0) всегда-всегда выполняется?"

Напишите программу, которая проверяет, что при любом x на входе тригонометрическое тождество будет выполняться (то есть будет выводить true при любом x).

Ограничение:
-1000 < x < 1000

Примечание:
Решение должно находиться в файле с названием Solution.java.
Публичный класс с решением должен называться Solution.
Использовать package нельзя.

Пример входных данных
90


Пример выходных данных
true
 */
public class Solution09 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int input = scanner.nextInt();
        scanner.close();

        double rad = Math.toRadians(input);
        if (Math.pow(Math.sin(rad),2) + Math.pow(Math.cos(rad),2) == 1)
            System.out.println("true");
        else
            System.out.println(false);
    }
}
