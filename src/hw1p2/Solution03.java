package hw1p2;

import java.util.Scanner;

/*
3. Время обедать
Петя снова пошел на работу.
С сегодняшнего дня он решил ходить на обед строго после полудня.
Периодически он посматривает на часы (x - час, который он увидел).
Помогите Пете решить, пора ли ему на обед или нет.
Если время больше полудня, то вывести "Пора". Иначе - “Рано”.

Ограничение:
0 <= n <= 23

Примечание:
Решение должно находиться в файле с названием Solution.java.
Публичный класс с решением должен называться Solution.
Использовать package нельзя.

Пример входных данных
7


Пример выходных данных
Рано
*/
public class Solution03 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int x = scanner.nextInt();
        scanner.close();

        String msgPositive = "Пора";
        String msgFail = "Рано";

        if (x > 12){
            System.out.println(msgPositive);
        } else{
            System.out.println(msgFail);
        }
    }
}
