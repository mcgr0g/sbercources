package hw1p2;

import java.util.Scanner;

/*
6. Уровень английского
На следующий день на работе Петю и его коллег попросили заполнить анкету.
Один из вопросов был про уровень владения английского.
Петя и его коллеги примерно представляют, сколько они знают иностранных слов.
Также у них есть табличка перевода количества слов в уровень владения английском языком.
Было бы здорово автоматизировать этот перевод!

На вход подается положительное целое число count - количество выученных иностранных слов.
Нужно вывести какому уровню соответствует это количество.

| Количество слов    | Уровень английского |
|----------------------|-----------------------|
| count < 500          | beginner              |
| 500 <= count < 1500  | pre-intermediate      |
| 1500 <= count < 2500 | intermediate          |
| 2500 <= count < 3500 | upper-intermediate    |
| 3500 <= count        | fluent                |

Ограничение:
0 <= count < 10000

Примечание:
Решение должно находиться в файле с названием Solution.java.
Публичный класс с решением должен называться Solution.
Использовать package нельзя.

Пример входных данных
595


Пример выходных данных
pre-intermediate
*/
public class Solution06 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int count = scanner.nextInt();
        scanner.close();

        if (count < 500)
            System.out.println("beginner");
        else if (500 <= count && count < 1500)
            System.out.println("pre-intermediate");
        else if (1500 <= count && count< 2500)
            System.out.println("intermediate");
        else if (2500 <= count && count< 3500)
            System.out.println("upper-intermediate");
        else if (3500 <= count)
            System.out.println("fluent");
    }
}
