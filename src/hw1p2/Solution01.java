package hw1p2;

import java.util.Scanner;

/*
1. Оценка на ревью
За каждый год работы Петя получает на ревью оценку.
На вход подаются оценки Пети за последние три года (три целых положительных числа).
Если последовательность оценок строго монотонно убывает, то вывести "Петя, пора трудиться"
В остальных случаях вывести "Петя молодец!"

Ограничение:
0 < a, b, c < 100

Примечание:
Решение должно находиться в файле с названием Solution.java.
Публичный класс с решением должен называться Solution.
Использовать package нельзя.

Пример входных данных
10 5 2


Пример выходных данных
Петя, пора трудиться
*/
public class Solution01 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int a = scanner.nextInt();
        int b = scanner.nextInt();
        int c = scanner.nextInt();
        scanner.close();
        
        String msgCool = "Петя молодец!";
        String msgFail = "Петя, пора трудиться";

        if (a > b && b > c){
            System.out.println(msgFail);
        } else{
            System.out.println(msgCool);
        }
    }
}
