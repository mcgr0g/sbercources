package hw1p2;

import java.util.Scanner;

/*
7. Разделение строки 1
Петя недавно изучил строки в джаве и решил попрактиковаться с ними.
Ему хочется уметь разделять строку по первому пробелу.
Для этого он может воспользоваться методами indexOf() и substring().

На вход подается строка. Нужно вывести две строки, полученные из входной разделением по первому пробелу.

Ограничение:
В строке гарантированно есть хотя бы один пробел
Первый и последний символ строки гарантированно не пробел
2 < s.length() < 100

Примечание:
Решение должно находиться в файле с названием Solution.java.
Публичный класс с решением должен называться Solution.
Использовать package нельзя.

Пример входных данных
Hi great team!


Пример выходных данных
Hi
great team!
*/
public class Solution07 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String input = scanner.nextLine();
        scanner.close();

        int position = input.indexOf(" ");
        String sub1 = input.substring(0, position);
        String sub2 = input.substring(position+1);
        
        System.out.println(sub1);
        System.out.println(sub2);
    }
}
