package hw3p1.task08;

public class Atm {
    private static int atmCounter;
    private double roublesPerDollar;
    private double dollarsPerRouble;

    private static final double ROUBLES_PER_DOLLAR_DEFAULT = 72.12;

    public Atm(double roublesPerDollar, double dollarsPerRouble) {
        if (roublesPerDollar <= 0 || dollarsPerRouble <= 0) {
            System.out.println("значения конвертации должны быть положительны");
            System.exit(-1);
        }
        this.roublesPerDollar = roublesPerDollar;
        this.dollarsPerRouble = dollarsPerRouble;
        atmCounter++;
    }

    public static int atmCount(){
        return atmCounter;
    }

    public double roublesToDollars(double roubles){
        return roubles * dollarsPerRouble;
    }

    public double dollarsToRoubles(double dollars) {
        return dollars * roublesPerDollar;
    }
}
