package hw3p1.task02;

import java.util.Arrays;

public class Student {
    private static final int MAX_COUNT_OF_GRADES = 10;

    private String name;
    private String surname;
    private int[] grades;
    private int gradesCount;

    public Student() {
        grades = new int[MAX_COUNT_OF_GRADES];
        gradesCount = 0;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getSurname() {
        return surname;
    }

    public void setGrades(int[] gradesToSave) {
        gradesCount = Math.min(gradesToSave.length, MAX_COUNT_OF_GRADES);
        System.arraycopy(gradesToSave, 0, this.grades, 0, gradesCount);
    }

    public int[] getGrades() {
        return grades;
    }

    public void addNewGrade(int gradeToAdd){
        System.arraycopy(grades, 1, grades, 0, MAX_COUNT_OF_GRADES-1);
//        System.out.println(Arrays.toString(grades));
        grades[MAX_COUNT_OF_GRADES-1] = gradeToAdd;
//        System.out.println(Arrays.toString(grades));
        if (gradesCount < MAX_COUNT_OF_GRADES)
            gradesCount++;
    }

    public double getAverageGrade(){
        double result = 0;
        for (int i = 0; i < MAX_COUNT_OF_GRADES; i++) {
            result += grades[i];
        }
        return result / gradesCount;
    }
}
