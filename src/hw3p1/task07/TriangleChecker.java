package hw3p1.task07;

public class TriangleChecker {
    public static boolean isPossibleToCompose(double a, double b, double c) {
        if (a+b>c &&  a+c>b && c+b>a)
            return true;
        return false;
    }
}
