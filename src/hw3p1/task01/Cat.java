package hw3p1.task01;

public class Cat {
    private static final int SLEEP_ACTION = 0;
    private static final int MEAOW_ACTION = 1;
    private static final int EAT_ACTION = 2;

    private static void sleep(){
        System.out.println("Sleep");
    }

    private static void meow(){
        System.out.println("Meow");
    }

    private static void eat(){
        System.out.println("Eat");
    }

    public static void status(){
        int actionChoice = (int) (Math.random() * 3);

        switch (actionChoice){
            case SLEEP_ACTION -> sleep();
            case MEAOW_ACTION -> meow();
            case EAT_ACTION -> eat();
        }
    }
}
