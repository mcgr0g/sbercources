package hw3p1.task04;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.InvalidPropertiesFormatException;

public class TimeUnit {

    private LocalTime chronometer;
    private static final DateTimeFormatter fmt24 = DateTimeFormatter.ofPattern("HH:mm:ss");
    private static final DateTimeFormatter fmt12 = DateTimeFormatter.ofPattern("hh:mm a");

    private static boolean isHoursValid(int hours) {
        return hours >= 0 && hours < 24;
    }

    private static boolean isMinutesValid(int minutes) {
        return minutes >= 0 && minutes < 60;
    }

    private static boolean isSecondsValid(int seconds) {
        return seconds >= 0 && seconds < 60;
    }

    public TimeUnit(int hours, int minutes, int seconds) throws Exception {
        if (isHoursValid(hours) && isMinutesValid(minutes) && isSecondsValid(seconds)) {
            chronometer = LocalTime.of(hours, minutes, seconds);
        } else {
            throw new Exception("Invalid time format: " + hours + ":" + minutes + ":" + seconds);
        }
    }

    public TimeUnit(int hours, int minutes) throws Exception {
        this(hours, minutes, 0);
    }

    public TimeUnit(int hours) throws Exception {
        this(hours, 0, 0);
    }

    public void printChronoInDefaultFormat() {
        System.out.println(chronometer.format(fmt24));
    }

    public void printChronoInAmericanFormat() {
        System.out.println(chronometer.format(fmt12));
    }

    public void timeCorrection(int hoursToAdd, int minutesToAdd, int secondsToAdd) {
        chronometer = chronometer.plusHours(hoursToAdd).plusMinutes(minutesToAdd).plusSeconds(secondsToAdd);
    }
}