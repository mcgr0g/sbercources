package hw3p1.task03;

import hw3p1.task02.Student;

import java.util.Arrays;

public class StudentService {
    public static Student bestStudent(Student[] studentsArray) {
        double bestAvgGrade = 0;
        int bestStudent = 0;

        for (int i = 0; i < studentsArray.length; i++) {
            if (studentsArray[i].getAverageGrade() >= bestAvgGrade) {
                bestStudent = i;
                bestAvgGrade = studentsArray[i].getAverageGrade();
            }
        }

        return studentsArray[bestStudent];
    }

    public static Student[] sortBySurname(Student[] studentsArray) {
        // хитрый компаратор можно скормить перегруженному sort()
        Arrays.sort(studentsArray, (a,b) -> a.getSurname().compareTo(b.getSurname()));

        return studentsArray;
    }
}
