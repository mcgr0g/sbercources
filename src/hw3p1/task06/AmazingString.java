package hw3p1.task06;

public class AmazingString {
    private char[] amazingString;

    public AmazingString(char[] newString) {
        int l = newString.length;
        amazingString = new char[l];
        System.arraycopy(newString, 0, amazingString, 0, l);
    }

    public AmazingString(String newString) {
        int l = newString.length();
        amazingString = new char[l];
        for (int i = 0; i < l; i++) {
            amazingString[i] = newString.charAt(i);
        }
    }

    public char getCharByIndex(int i) {
        return amazingString[i];
    }

    public int getLength() {
        return amazingString.length;
    }

    public void printIt() {
        for (char element : amazingString
        ) {
            System.out.print(element);
        }
        System.out.println();
    }

    public boolean isSubstring(char[] searchElementSequene) {
        int positionToCheck = 0;
        int subLength = searchElementSequene.length;
        int amzLength = amazingString.length;

        for (int i = 0; i < amzLength; i++) {
            if (amazingString[i] == searchElementSequene[positionToCheck]) {
                for (positionToCheck = 1; positionToCheck < subLength; positionToCheck++) {

                    if (i + positionToCheck >= amzLength
                            || !(amazingString[i + positionToCheck] == searchElementSequene[positionToCheck])
                    )
                        break;

                    if (positionToCheck == subLength - 1)
                        return true;
                }
                positionToCheck = 0;
            }
        }
        return false;
    }

    public boolean isSubstring(String searchString) {
        int positionToCheck = 0;
        int subLength = searchString.length();
        int amzLength = amazingString.length;

        for (int i = 0; i < amzLength; i++) {
            if (amazingString[i] == searchString.charAt(positionToCheck)) {
                for (positionToCheck = 1; positionToCheck < subLength; positionToCheck++) {

                    if (i + positionToCheck >= amzLength
                            || !(amazingString[i + positionToCheck] == searchString.charAt(positionToCheck))
                    )
                        break;

                    if (positionToCheck == subLength - 1)
                        return true;
                }
                positionToCheck = 0;
            }
        }
        return false;
    }

    public void removeLeaderSpaces() {
        int amzLength = amazingString.length;
        int badCounter = 0;
        for (char c : amazingString) {
            if (c == ' ')
                badCounter++;
        }
        char[] newString = new char[amzLength-badCounter];
        System.arraycopy(amazingString, badCounter, newString, 0, newString.length);
//        System.out.println(Arrays.toString(newString));
        this.amazingString = newString;
    }

    public void reverse() {
        int amzLength = amazingString.length;
        char buff;
        for (int i = 0; i < amzLength / 2; i++) {
            buff = amazingString[i];
            amazingString[i] = amazingString[amzLength-1-i];
            amazingString[amzLength-1-i] = buff;
        }
    }

}
