package hw3p1.task05;

public class Main {
    public static void main(String[] args) {
//        DayOfWeek[] days = new DayOfWeek[7];
        DayOfWeek[] daysArr = {
                new DayOfWeek(1, "Monday"),
                new DayOfWeek(2, "Tuesday"),
                new DayOfWeek(3, "Wednesday"),
                new DayOfWeek(4, "Tuesday"),
                new DayOfWeek(5, "Friday"),
                new DayOfWeek(6, "Saturday"),
                new DayOfWeek(7, "Sunday"),
        };

        for (DayOfWeek day: daysArr
             ) {
            System.out.println(day.getIdx() + " " + day.getDay());
        }
    }
}
