package hw3p1.task05;

public class DayOfWeek {
    private byte idx;
    private String day;


    public DayOfWeek(int idx, String day) {
        this.idx = (byte) idx;
        this.day = day;
    }

    public byte getIdx() {
        return idx;
    }

    public String getDay() {
        return day;
    }
}
