package hw2p2;

import java.util.Scanner;
/*На вход подается число N — количество строк и столбцов матрицы. Затем
передаются координаты X и Y расположения коня на шахматной доске.
Необходимо заполнить матрицу размера NxN нулями, местоположение коня
отметить символом K, а позиции, которые он может бить, символом X.
*/
public class Solution03 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int rowLimit = scanner.nextInt();

        int[][] arr = new int[rowLimit][rowLimit];

        //первая коня
        int columnFirst = scanner.nextInt();
        int rowFirst = scanner.nextInt();

        for (int row = 0; row < rowLimit; row++){
            for (int column = 0; column < rowLimit; column++) {
                if (row == rowFirst && column == columnFirst)
                    arr[row][column] = -1;
                else if (//движение против часовой, как углы отмеряют
                        (row == rowFirst - 1 && column == columnFirst +2)//первый квадрант раз
                        ||(row == rowFirst - 2 && column == columnFirst +1)//первый квадрант два
                        ||(row == rowFirst - 2 && column == columnFirst -1)//второй квадрант раз
                        ||(row == rowFirst - 1 && column == columnFirst -2)//второй квадрант два
                        ||(row == rowFirst + 1 && column == columnFirst -2)//третий квадрант раз
                        ||(row == rowFirst + 2 && column == columnFirst -1)//третий квадрант два
                        ||(row == rowFirst + 2 && column == columnFirst +1)//четвертый квадрант раз
                        ||(row == rowFirst + 1 && column == columnFirst +2)//четвертый квадрант два
                )
                    arr[row][column] = 1;
                else
                    arr[row][column] = 0;
            }
        }
        for (int row = 0; row < rowLimit; row++) {
            for (int column = 0; column < rowLimit; column++) {
                String append = " ";
                if (column == rowLimit - 1)
                    append = "";
                String printSymbol = "0";
                if (arr[row][column] > 0)
                    printSymbol = "X";
                else if (arr[row][column] < 0)
                    printSymbol = "K";
                System.out.print(printSymbol + append);
            }
            System.out.println();
        }
    }
}
