package hw2p2;

import java.util.Scanner;

/*
На вход подается число N — количество строк и столбцов матрицы. Затем в
последующих двух строках подаются координаты X (номер столбца) и Y (номер
строки) точек, которые задают прямоугольник.
Необходимо отобразить прямоугольник с помощью символа 1 в матрице,
заполненной нулями (см. пример) и вывести всю матрицу на экран.
* */
public class Solution02 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int rowLimit = scanner.nextInt();

        int[][] arr = new int[rowLimit][rowLimit];

        //первая точка
        int columnFirst = scanner.nextInt();
        int rowFirst = scanner.nextInt();
        //вторая точка
        int columnSecond = scanner.nextInt();
        int rowSecond = scanner.nextInt();


        for (int row = 0; row < rowLimit; row++){
            for (int column = 0; column < rowLimit; column++) {
                if (
                    (row == rowFirst && column >= columnFirst && column <= columnSecond)
                    ||
                    (row == rowSecond && column >= columnFirst && column <= columnSecond)
                    ||
                    (column == columnFirst && row >= rowFirst && row <= rowSecond)
                    ||
                    (column == columnSecond && row >= rowFirst && row <= rowSecond)
                )
                    arr[row][column] = 1;
                else
                    arr[row][column] = 0;
            }
        }
        for (int row = 0; row < rowLimit; row++) {
            for (int column = 0; column < rowLimit; column++) {
                String append = " ";
                if (column == rowLimit - 1)
                    append = "";
                System.out.print(arr[row][column] + append);
            }
            System.out.println();
        }
    }
}
