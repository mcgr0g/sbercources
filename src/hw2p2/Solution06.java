package hw2p2;

import java.util.Scanner;

/*
Петя решил начать следить за своей фигурой. Но все существующие
приложения для подсчета калорий ему не понравились и он решил написать
свое. Петя хочет каждый день записывать сколько белков, жиров, углеводов и
калорий он съел, а в конце недели приложение должно его уведомлять,
вписался ли он в свою норму или нет.
На вход подаются числа A — недельная норма белков, B — недельная норма
жиров, C — недельная норма углеводов и K — недельная норма калорий.
Затем передаются 7 строк, в которых в том же порядке указаны сколько было
съедено Петей нутриентов в каждый день недели. Если за неделю в сумме по
каждому нутриенту не превышена недельная норма, то вывести “Отлично”,
иначе вывести “Нужно есть поменьше”.
*/
public class Solution06 {
    public static final String msgPositive = "Отлично";
    public static final String msgNegative = "Нужно есть поменьше";
    public static final int weekLength = 7;
    public static final int limitItemCount = 4;

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
//        int weekLimitProtein = scanner.nextInt();
//        int weekLimitFat = scanner.nextInt();
//        int weekLimitCarbohydrate = scanner.nextInt();
//        int weekLimitCalories = scanner.nextInt();

        int[] weekLimit = new int[limitItemCount];
        for (int limitItem = 0; limitItem < 4; limitItem++) {
            weekLimit[limitItem] = scanner.nextInt();
        }

        int[][] weekConsumption = new int[weekLength][4];

        for (int day = 0; day < weekLength; day++) {
            for (int limitItem = 0; limitItem < limitItemCount; limitItem++) {
                weekConsumption[day][limitItem] = scanner.nextInt();
            }
        }

        boolean result = true;


        for (int limitItem = 0; limitItem < limitItemCount; limitItem++) {
            int sum = 0;
            for (int day = 0; day < weekLength; day++) {
                sum += weekConsumption[day][limitItem];
            }
            if (sum > weekLimit[limitItem])
                result = false;
        }

        if (result)
            System.out.println(msgPositive);
        else
            System.out.println(msgNegative);
    }
}
