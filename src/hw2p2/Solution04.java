package hw2p2;

import java.util.Scanner;
/*
На вход подается число N — количество строк и столбцов матрицы. Затем
передается сама матрица, состоящая из натуральных чисел. После этого
передается натуральное число P.
Необходимо найти элемент P в матрице и удалить столбец и строку его
содержащий (т.е. сохранить и вывести на экран массив меньшей размерности).
Гарантируется, что искомый элемент единственный в массиве.
*/
public class Solution04 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int rowLimit = scanner.nextInt();

        int[][] arr = new int[rowLimit][rowLimit];
        for (int row = 0; row < rowLimit; row++){
            for (int column = 0; column < rowLimit; column++) {
                arr[row][column] = scanner.nextInt();
            }
        }

        int valueForTurkishAnnihilation = scanner.nextInt();
        int rowToDel = 0, columnToDel = 0;

        outerloop:
        for (int row = 0; row < rowLimit; row++){
            for (int column = 0; column < rowLimit; column++) {
                if (arr[row][column] == valueForTurkishAnnihilation){
                    rowToDel = row;
                    columnToDel = column;
                    break outerloop;
                }
            }
        }


        int[][] resultArr = new int[rowLimit-1][rowLimit-1];
        int newRowIndex =0, newColumnIndex = 0;
        boolean rowDeleted = false;

        for (int row = 0; row < rowLimit; row++){

            for (int column = 0; column < rowLimit; column++) {
                if (rowToDel == row) {
                    rowDeleted = true;
                    continue;
                }
                else if (column == columnToDel ){
                    continue;
                } else {
                    resultArr[newRowIndex][newColumnIndex] = arr[row][column];
                    newColumnIndex++;
                }
            }
            if (!rowDeleted)
                newRowIndex++;
            newColumnIndex = 0;
        }

        for (int row = 0; row < rowLimit-1; row++) {
            for (int column = 0; column < rowLimit-1; column++) {
                String append = " ";
                if (column == rowLimit - 2)
                    append = "";
                System.out.print(resultArr[row][column] + append);
            }
            System.out.println();
        }
    }
}
