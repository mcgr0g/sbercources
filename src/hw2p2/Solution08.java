package hw2p2;

import java.util.Scanner;

/*
На вход подается число N. Необходимо посчитать и вывести на экран сумму его
цифр. Решить задачу нужно через рекурсию.
*/
public class Solution08 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        scanner.close();

        System.out.println(digitSum(n, 0));
    }

    private static int digitSum(int number, int prevResult) {
        if ( number <= 0)
            return prevResult;
        return digitSum(number /10, prevResult + number % 10);
    }
}
