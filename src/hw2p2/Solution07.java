package hw2p2;

import java.util.Scanner;

/*
Раз в год Петя проводит конкурс красоты для собак. К сожалению, система
хранения участников и оценок неудобная, а победителя определить надо. В
первой таблице в системе хранятся имена хозяев, во второй - клички животных,
в третьей — оценки трех судей за выступление каждой собаки. Таблицы
связаны между собой только по индексу. То есть хозяин i-ой собаки указан в i-ой
строке первой таблицы, а ее оценки — в i-ой строке третьей таблицы. Нужно
помочь Пете определить топ 3 победителей конкурса.
На вход подается число N — количество участников конкурса. Затем в N
строках переданы имена хозяев. После этого в N строках переданы клички
собак. Затем передается матрица с N строк, 3 вещественных числа в каждой —
оценки судей. Победителями являются три участника, набравшие
максимальное среднее арифметическое по оценкам 3 судей. Необходимо
вывести трех победителей в формате “Имя хозяина: кличка, средняя оценка”.
Гарантируется, что среднее арифметическое для всех участников будет
различным.
*/
public class Solution07 {
    public static final int MASTER_NAME_INDEX = 0;
    public static final int PET_NAME_INDEX = 1;
    public static final int COUNT_OF_JUDGES = 3;
    public static final int TOTAL_ATTR_COUNT = COUNT_OF_JUDGES + 3;
    public static final int LAST_ATTR_INDEX = TOTAL_ATTR_COUNT - 1;

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int numberOfCompetitors = scanner.nextInt(); scanner.nextLine();
        Object[][] competitorsArr = new Object[numberOfCompetitors][TOTAL_ATTR_COUNT];

        for (int competitor = 0; competitor < numberOfCompetitors; competitor++) {
            competitorsArr[competitor][MASTER_NAME_INDEX] = scanner.nextLine();
        }
        for (int competitor = 0; competitor < numberOfCompetitors; competitor++) {
            competitorsArr[competitor][PET_NAME_INDEX] = scanner.nextLine();
        }

        for (int competitor = 0; competitor < numberOfCompetitors; competitor++) {
            double result = 0;
            for (int judgeIndex = 0; judgeIndex < COUNT_OF_JUDGES; judgeIndex++) {
                double judgeScore = scanner.nextDouble();
                competitorsArr[competitor][judgeIndex+2] = judgeScore;
                result += judgeScore;

            }
            competitorsArr[competitor][LAST_ATTR_INDEX] = result/COUNT_OF_JUDGES;
        }

        //теперь надо пересортировать массив по LAST_ATTR_INDEX
        for (int i = 0; i < numberOfCompetitors; i++) {
            for (int j = 0; j < numberOfCompetitors-1; j++) {
                double firstInPair = (Double) competitorsArr[j][LAST_ATTR_INDEX];
                double secondInPair = (Double) competitorsArr[j+1][LAST_ATTR_INDEX];

                if (firstInPair < secondInPair)
                    swap(competitorsArr, j, j+1);
            }

        }

        for (int competitor = 0; competitor < 3; competitor++) {
            System.out.print(competitorsArr[competitor][MASTER_NAME_INDEX] + ": ");
            System.out.print(competitorsArr[competitor][PET_NAME_INDEX] + ", ");

            double result =(int) ((double)(competitorsArr[competitor][LAST_ATTR_INDEX])*10) / 10.0;
            System.out.print(result);
            System.out.println();
        }
    }

    private static void swap(Object[][] competitorsArr, int i, int j) {
        // Обмениваем строки местами
        // Итерируемся по элементам двух строк
        for (int k = 0; k < competitorsArr[i].length; k++) {
            // буферный элемент в котором хранится значение элемента массива строки с индексом i
            Object buff = competitorsArr[i][k];
            // в ячейку строки с индексом i записываем значение из ячейки строки с индексом j той же колонки
            competitorsArr[i][k] = competitorsArr[j][k];
            // в ячейку строки с индексом j записываем значение из буфера
            competitorsArr[j][k] = buff;
        }
    }
}
