package hw2p2;

import java.util.Scanner;

/*
На вход подается число N — количество строк и столбцов матрицы.
Затем передается сама матрица, состоящая из натуральных чисел.
Необходимо вывести true, если она является симметричной относительно
побочной диагонали, false иначе.
Побочной диагональю называется диагональ, проходящая из верхнего правого
угла в левый нижний.
*/
public class Solution05 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int rowLimit = scanner.nextInt();

        int[][] arr = new int[rowLimit][rowLimit];
        for (int row = 0; row < rowLimit; row++){
            for (int column = 0; column < rowLimit; column++) {
                arr[row][column] = scanner.nextInt();
            }
        }
        boolean result = true;

        for (int row = 0; row < rowLimit; row++) {
            for (int column = rowLimit -1 ; column >= 0; column--) {
                if(arr[row][column] != arr[rowLimit -1 - column][rowLimit -1 - row])
                    result = false;
            }
        }
        System.out.println(result);
    }
}
