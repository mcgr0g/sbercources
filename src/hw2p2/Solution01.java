package hw2p2;

import java.util.Scanner;

/*
На вход передается N — количество столбцов в двумерном массиве и M —
количество строк. Затем сам передается двумерный массив, состоящий из
натуральных чисел.
Необходимо сохранить в одномерном массиве и вывести на экран
минимальный элемент каждой строки.
*/
public class Solution01 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int columnLimit = scanner.nextInt();
        int rowLimit = scanner.nextInt();

        int[][] arr = new int[rowLimit][columnLimit];

        for (int row = 0; row < rowLimit; row++){
            for (int column = 0; column < columnLimit; column++) {
                arr[row][column] = scanner.nextInt();
            }
        }

        int[] resultArr = new int[rowLimit];

        for (int row = 0; row < rowLimit; row++) {
            resultArr[row] = arr[row][0];
        }

        for (int row = 0; row < rowLimit; row++) {
            for (int column = 1; column < columnLimit; column++) {
                resultArr[row] = Math.min(arr[row][column], resultArr[row]);
            }
        }
        for (int element:
             resultArr) {
            System.out.println(element);
        }
    }
}
