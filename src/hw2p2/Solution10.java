package hw2p2;

import java.util.Scanner;

/*
На вход подается число N. Необходимо вывести цифры числа справа налево.
Решить задачу нужно через рекурсию.
*/
public class Solution10 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        scanner.close();

        System.out.println(reverseDigits(n));
    }

    private static String reverseDigits(int n) {
        return reverseDigits(n, "");
    }

    private static String reverseDigits(int numberHead, String s) {
        if (numberHead <=0 )
            return s;
        String delimeter;
        if (s == "")
            delimeter = "";
        else delimeter = " ";
        return reverseDigits(numberHead / 10,   s + delimeter +  (numberHead % 10));
    }
}
