package hw1p2s;

import java.util.Scanner;

/*
2. Проверка посылки
У нас есть почтовая посылка (String mailPackage).
Каждая почтовая посылка проходит через руки проверяющего.
Работа проверяющего заключается в следующем:
 во-первых, посмотреть не пустая ли посылка;
 во-вторых, проверить нет ли в ней камней или запрещенной продукции.

Наличие камней или запрещенной продукции указывается в самой посылке в конце или в начале.
Если в посылке есть камни, то будет написано слово "камни!", если запрещенная продукция, то будет фраза "запрещенная продукция".
После осмотра посылки проверяющий должен сказать:
 "камни в посылке" – если в посылке есть камни;
 "в посылке запрещенная продукция" – если в посылке есть что-то запрещенное;
 "в посылке камни и запрещенная продукция" – если в посылке находятся камни и запрещенная продукция;
 "все ок" – если с посылкой все хорошо.

Если посылка пустая, то с посылкой все хорошо.
Напишите программу, которая будет заменять проверяющего.

Примечание:
Решение должно находиться в файле с названием Solution.java.
Публичный класс с решением должен называться Solution.
Использовать package нельзя.

Пример входных данных
мед. чайник.


Пример выходных данных
все ок
*/

public class Solution2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String mailPackage = scanner.nextLine();
        scanner.close();

        String strRock = "камни!";
        String strZap = "запрещенная продукция";

        boolean hasRocks = false;
        boolean hasZapresenka = false;

        if (mailPackage.startsWith(strRock) || mailPackage.endsWith(strRock))
            hasRocks = true;
        if (mailPackage.startsWith(strZap) || mailPackage.endsWith(strZap))
            hasZapresenka = true;

        if (!hasRocks && !hasZapresenka) {
            System.out.println("все ок");
        } else if (hasRocks && hasZapresenka) {
            System.out.println("в посылке камни и запрещенная продукция");
        } else if (hasRocks && !hasZapresenka) {
            System.out.println("камни в посылке");
        } else if (!hasRocks && hasZapresenka) {
            System.out.println("в посылке запрещенная продукция");
        }
    }
}
