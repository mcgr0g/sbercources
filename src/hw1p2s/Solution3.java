package hw1p2s;

import java.util.Scanner;

/*
3. Покупка телефона
Старый телефон Андрея сломался, поэтому он решил приобрести новый.
Продавец телефонов предлагает разные варианты, но Андрея интересуют только модели серии samsung или iphone.
Также Андрей решил рассматривать телефоны только от 50000 до 120000 рублей.
Чтобы не тратить время на разговоры, Андрей хочет написать программу, которая поможет ему сделать выбор.

На вход подается строка – модель телефона и число – стоимость телефона.
Нужно вывести "Можно купить", если модель содержит слово samsung или iphone и стоимость от 50000 до 120000 рублей включительно.
Иначе вывести "Не подходит".

Гарантируется, что в модели телефона не указано одновременно несколько серий.

Примечание:
Решение должно находиться в файле с названием Solution.java.
Публичный класс с решением должен называться Solution.
Использовать package нельзя.

Пример входных данных
iphone XL
58000


Пример выходных данных
Можно купить
 */

public class Solution3 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String model = scanner.nextLine();
        int price = scanner.nextInt();
        scanner.close();

        boolean isPrettyModel = model.contains("iphone") || model.contains("samsung");

        boolean isPrerryPrice = (50000 <= price) && (price <= 120000);

        if (isPrerryPrice && isPrettyModel)
            System.out.println("Можно купить");
        else
            System.out.println("Не подходит");
    }
}
