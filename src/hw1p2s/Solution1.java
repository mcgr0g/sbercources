package hw1p2s;

import java.util.Scanner;
/*
1. Проверить пароль
У Марата был взломан пароль.
Он решил написать программу, которая проверяет его пароль на сложность.

В интернете он узнал, что пароль должен отвечать следующим требованиям:
 пароль должен состоять из хотя бы 8 символов
В пароле должны быть:
 заглавные буквы
 строчные символы
 числа
 специальные знаки (_-)

Если пароль прошел проверку, то программа должна вывести в консоль строку пароль надежный, иначе строку: пароль не прошел проверку

Примечание:
Решение должно находиться в файле с названием Solution.java.
Публичный класс с решением должен называться Solution.
Использовать package нельзя.

Пример входных данных
Hello_22


Пример выходных данных
пароль надежный
*/

public class Solution1 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String passwd = scanner.nextLine();
        scanner.close();

        String msgPositive = "пароль надежный";
        String msgNegative = "пароль не прошел проверку";
        char ch;
        boolean capitalFlag = false;
        boolean lowerCaseFlag = false;
        boolean numberFlag = false;
        boolean specialFlag = false;

        for (int i=0; i<passwd.length() ;i++){
            ch = passwd.charAt(i);
            if( Character.isDigit(ch)) {
                numberFlag = true;
            } else if (Character.isUpperCase(ch)) {
                capitalFlag = true;
            } else if (Character.isLowerCase(ch)) {
                lowerCaseFlag = true;
            } else if (ch == '_' || ch == '*' || ch == '-'){
                specialFlag = true;
            }

        }

        if (
                passwd.length() >=8
                && lowerCaseFlag
                && capitalFlag
                && numberFlag
                && specialFlag
        ){
            System.out.println(msgPositive);
        } else
            System.out.println(msgNegative);
    }

}
