package hw2p1s;

import java.util.Arrays;
import java.util.Scanner;
/*
На вход подается число N — длина массива. Затем передается массив
целых чисел (ai) из N элементов, отсортированный по возрастанию.
 за линейное время со сложностью O(n)

Необходимо вывести на экран построчно сколько встретилось различных
элементов.
Каждая строка должна содержать количество элементов и сам
элемент через пробел.
*/

public class Solutoin2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[] arr = new int[n];
        for (int i=0; i < n; i++)
            arr[i] = scanner.nextInt();
        scanner.close();

        int indexOfFirstPositive = -1; // будет влиять на выбранную стратегию перезаписывания целевого массива
        if (arr[0] >= 0)
            indexOfFirstPositive = 0;

        for (int i=1; i < n; i++) {
            if (arr[i-1] < 0 && arr[i] >= 0) {
                indexOfFirstPositive = i;
            }
        }

        if (indexOfFirstPositive == 0) {
            // отрицательных не было вообще
            // просто возведем все в квадрат
            for (int i = 0; i < n; i++) {
                arr[i] *= arr[i];
            }
        } else {
            int[] arrWithNegativeSource;
            if (indexOfFirstPositive < 0) {
                // все значения были отрицательные
                // после возведения в квадрат надо развернуть массив
                arrWithNegativeSource = new int[n];
                for (int i = 0; i < n ; i++) {
                    arrWithNegativeSource[i] = arr[i] * arr[i];
                }
                for (int i = 0; i < n; i++) {
                    arr[i] = arrWithNegativeSource[n-1-i];
                }
            } else {
                // есть микс отрицательных и положительных в исходном массиве
                // надо все аккуратно разложить на 2 массива, потом смержить их

                int[] arrWithPositiveSource = new int[n-indexOfFirstPositive];
                for (int i = indexOfFirstPositive, iOfPositiveArr = 0; i < n; i++, iOfPositiveArr++) {
                    arrWithPositiveSource[iOfPositiveArr] = arr[i] * arr[i];
                }

                arrWithNegativeSource = new int[indexOfFirstPositive];
                for (int i = 0; i < indexOfFirstPositive ; i++) {
                    // не забываем разворачивать из источника отрицательных чисел
                    arrWithNegativeSource[indexOfFirstPositive - 1 - i] = arr[i] * arr[i];
                }

//                System.out.println(Arrays.toString(arrWithPositiveSource));
//                System.out.println(Arrays.toString(arrWithNegativeSource));
                mergeTwoArraysInOne(arrWithPositiveSource, arrWithNegativeSource, arr);
            }
        }

        for (int elem : arr) {
            System.out.print(elem + " ");
        }
    }
    public static void mergeTwoArraysInOne(int[] arr1, int[] arr2, int[] mergedArray) {
        int i = 0, j = 0, k = 0;

        //обход двух массивов
        while (i < arr1.length && j < arr2.length) {
            if (arr1[i] < arr2[j]) {
                mergedArray[k++] = arr1[i++];
            } else {
                mergedArray[k++] = arr2[j++];
            }
        }
        //сохраняем оставшиеся элементы первого массива
        while (i < arr1.length) {
            mergedArray[k++] = arr1[i++];
        }

        //сохраняем оставшиеся элементы второго массива
        while (j < arr2.length) {
            mergedArray[k++] = arr2[j++];
        }
        System.out.println(Arrays.toString(mergedArray));
    }
}
