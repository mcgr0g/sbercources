package hw2p1s;

import java.util.Arrays;
import java.util.Scanner;

/*
Создать программу генерирующую пароль.
На вход подается число N — длина желаемого пароля. Необходимо проверить,
что N >= 8, иначе вывести на экран "Пароль с N количеством символов
небезопасен" (подставить вместо N число) и предложить пользователю еще раз
ввести число N.
Если N >= 8 то сгенерировать пароль, удовлетворяющий условиям ниже и
вывести его на экран. В пароле должны быть:
заглавные латинские символы
строчные латинские символы
числа
специальные знаки(_*-)
*/
public class Solution1 {

    public static final int REQUIREMET_DIGIT_ORDER = 0;
    public static final int REQUIREMENT_SPECH_ORDER = 1;
    public static final int REQUIREMENT_CAPITAL_ORDER = 2;
    public static final int REQUIREMENT_LOWC_ORDER = 3;

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int passwdLength = scanner.nextInt();
        while (passwdLength < 8) {
            System.out.println("Пароль с N количеством символов небезопасен");
            passwdLength = scanner.nextInt();
        }
        scanner.close();
        System.out.println(genPass(passwdLength));
    }

    public static String genPass(int length) {
        String finalString = "";
        //initial
        finalString = appendViaChoise(finalString, (int) (Math.random()*4));

        for (int i = 1; i < length; i++) {
            int cubeEdge = -1;
            for (int requirementOrder = 0; requirementOrder < 4; requirementOrder++){
                if (!checkOrderedRequirement(finalString)[requirementOrder]){
                    cubeEdge = requirementOrder;
                    //берем первое не выбранное требование к строке
                    break;
                }
            }

            if (cubeEdge == -1)
                cubeEdge = (int) (Math.random()*4);

            finalString = appendViaChoise(finalString, cubeEdge);
        }
//        System.out.println(Arrays.toString(checkOrderedRequirement(finalString)));
        return finalString;
    }

    public static String appendViaChoise(String baseToappend, int choice){
//        choice++;//рандом работает от 1, а вот массив с проверками требований - от 0
        switch (choice) {
            case (REQUIREMET_DIGIT_ORDER) -> baseToappend += genDigit();
            case (REQUIREMENT_SPECH_ORDER) -> baseToappend += genSpecCar();
            case (REQUIREMENT_CAPITAL_ORDER) -> baseToappend += genCapitalLetter();
            case (REQUIREMENT_LOWC_ORDER) -> baseToappend += genLowLetter();
        }
        return baseToappend;
    }

    public static boolean[] checkOrderedRequirement(String passwd){
        char ch;
        boolean[] flagsArray = new boolean[4];
        for (int i=0; i<passwd.length() ;i++){
            ch = passwd.charAt(i);
            if( Character.isDigit(ch)) {
                flagsArray[REQUIREMET_DIGIT_ORDER] = true;
            } else if (Character.isUpperCase(ch)) {
                flagsArray[REQUIREMENT_CAPITAL_ORDER] = true;
            } else if (Character.isLowerCase(ch)) {
                flagsArray[REQUIREMENT_LOWC_ORDER] = true;
            } else if (ch == '_' || ch == '*' || ch == '-'){
                flagsArray[REQUIREMENT_SPECH_ORDER] = true;
            }

        }
        return flagsArray;
    }

    private static char genSpecCar() {
        int symbol = (int) (Math.random() * 3);
        char result = switch (symbol) {
            case (0) -> '_';
            case (1) -> '*';
            case (2) -> '-';
            default -> '!';
        };
        return result;
    }

    private static char genDigit() {
        return (char) (
                (int) (Math.random() * 9 + '0')
        );
    }

    public static char genCapitalLetter() {
        return (char) (
                (int) (Math.random() * 26) + 'A'
        );
    }

    public static char genLowLetter() {
        return (char) (
                (int) (Math.random() * 26) + 'a'
        );
    }
}
