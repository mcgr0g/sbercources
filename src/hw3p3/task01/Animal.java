package hw3p3.task01;

public abstract class Animal {

    public abstract void wayOfBirth();

    public final void toEat(){
        System.out.println("все животные одинаково едят");
    }

    public final void toSleep(){
        System.out.println("все животные одинаково спят");
    }
}
