package hw3p3.task04;

import java.util.ArrayList;

public class Participant {
    private String name;
    private Dog dog;

    Participant(String name){
        this.name = name;
    }

    public String getName(){
        return name;
    }

    public void setPet(String petName){
        this.dog = new Dog(petName);
    }

    public String getPetName(){
        return dog.getName();
    }

    public boolean setPetScore(ArrayList<Double> scores){
        return dog.setScores(scores);
    }

    public double getAVGScore(){
        return dog.getAVGScore();
    }
}
