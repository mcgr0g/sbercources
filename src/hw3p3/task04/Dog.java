package hw3p3.task04;

import java.util.ArrayList;

public class Dog {
    private String name;
    private ArrayList<Double> scores;
    final static int SCORE_COUNT = 3;

    Dog(String name){
        this.name = name;
        scores = new ArrayList<>(SCORE_COUNT);
    }

    public String getName() {
        return name;
    }

    /*
    * false - не удалось сохранить оценки жюри
    * true - удалось
    * */
    public boolean setScores(ArrayList<Double> scoreSet){
        if (scoreSet.size() != SCORE_COUNT){
            System.out.println("некорректное количество оценок");
            return false;
        }
        for (int i = 0; i < SCORE_COUNT; i++) {
            scores.add(scoreSet.get(i));
        }
        return true;
    }

    public double getAVGScore(){
        double result = 0;
        for (int i = 0; i < SCORE_COUNT; i++) {
            result += scores.get(i);
        }
        return (int) (result/SCORE_COUNT * 10 ) / 10.0 ;
    }

}
