package hw3p3.task04;

import java.util.*;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int numberOfCompetitors = scanner.nextInt(); scanner.nextLine();

        ArrayList<Participant> competitorsList = new ArrayList<>(numberOfCompetitors);

        for (int i = 0; i < numberOfCompetitors; i++) {
            competitorsList.add(new Participant(scanner.nextLine()));
        }

        for (int i = 0; i < numberOfCompetitors; i++) {
            competitorsList.get(i).setPet(scanner.nextLine());
        }

        for (int i = 0; i < numberOfCompetitors; i++) {
            ArrayList<Double> scoreTemp = new ArrayList<>(Dog.SCORE_COUNT);
            for (int j = 0; j < Dog.SCORE_COUNT; j++) {
                scoreTemp.add(scanner.nextDouble());
            }
            competitorsList.get(i).setPetScore(scoreTemp);
        }

        competitorsList.sort(Comparator.comparingDouble(Participant::getAVGScore).reversed());


        for (int i = 0; i < numberOfCompetitors; i++) {
            String str = competitorsList.get(i).getName() + ": " +
                    competitorsList.get(i).getPetName() + " " +
                    competitorsList.get(i).getAVGScore();
            System.out.println(str);
        }

    }
}
