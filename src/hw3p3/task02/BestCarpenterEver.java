package hw3p3.task02;

public class BestCarpenterEver {

    public boolean isPossibleToFix(Furniture furniture){
        return furniture instanceof StoolFurniture;
    }
}
