package hw3p3.task02;

public class Test {
    public static void main(String[] args) {
//        TableFurniture table = new TableFurniture();
        Furniture stool = FurnitureFactory.getFurniture(FurnitureType.STOOL);
        Furniture table = FurnitureFactory.getFurniture(FurnitureType.TABLE);

        BestCarpenterEver workshop = new BestCarpenterEver();

        System.out.println(workshop.isPossibleToFix(table));
        System.out.println(workshop.isPossibleToFix(stool));
    }
}
