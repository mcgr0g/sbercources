package hw3p3.task02;

public class FurnitureFactory {
    public static Furniture getFurniture(FurnitureType type){
        Furniture furniture = null;

        switch (type){
            case STOOL -> furniture = new StoolFurniture();
            case TABLE -> furniture = new TableFurniture();
        }

        return furniture;
    }
}
