package hw3p3.task03;

import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int columnCount = scanner.nextInt();
        int rowCount = scanner.nextInt();
        scanner.close();

        ArrayList<ArrayList<Integer>> matrix = new ArrayList<>(columnCount);

        for (int i = 0; i < columnCount; i++) {
            matrix.add(new ArrayList<>(rowCount));//добавим пустую колонку высотой rowCount
            for (int j = 0; j < rowCount; j++) {
                int value = i + j;
                matrix.get(i).add(value);
            }
        }

        for (int i = 0; i < rowCount; i++) {
            for (int j = 0; j < columnCount; j++) {
                String delimeter = " ";
                int temp = matrix.get(j).get(i);
                if (j == columnCount - 1)
                    delimeter = "\n";
                System.out.print(temp + delimeter);
            }
        }
    }
}
