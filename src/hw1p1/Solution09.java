package hw1p1;

import java.util.Scanner;

/*
На вход подается бюджет мероприятия – n тугриков. Бюджет на одного гостя – k
тугриков. Вычислите и выведите, сколько гостей можно пригласить на
мероприятие.
*/
public class Solution09 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int totalBudjet = scanner.nextInt();

        short guestBudjet = scanner.nextShort();
        scanner.close();

        System.out.println((short) (totalBudjet / guestBudjet));
    }
}