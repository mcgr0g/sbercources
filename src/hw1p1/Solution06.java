package hw1p1;

import java.util.Scanner;
/*На вход подается количество километров count. Переведите километры в мили
(1 миля = 1,60934 км) и выведите количество миль
*/
public class Solution06 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        short count = scanner.nextShort();
        scanner.close();
        
        final double DENUMERATOR = 1.60934;
        System.out.println( count / DENUMERATOR);
    }
}
