package hw1p1;

import java.util.Scanner;

/*
Вычислите и выведите на экран объем шара, получив его радиус r с консоли.
Подсказка: считать по формуле V = 4/3 * pi * r^3. Значение числа pi взять из
Math.
*/
public class Solution01 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        float radius = scanner.nextFloat();
        scanner.close();
        
        double volume = 4.0/3 * Math.PI * Math.pow(radius, 3);
        System.out.println(volume);
    }
}
