package hw1p1;

import java.util.Scanner;
/*
На вход подается два целых числа a и b. Вычислите и выведите среднее квадратическое a и b.

*/

public class Solution02 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        byte a = scanner.nextByte();
        byte b = scanner.nextByte();
        scanner.close();
        double s = Math.sqrt((a*a + b*b)/2);
        System.out.println(s);
    }
}
