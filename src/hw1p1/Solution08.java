package hw1p1;

import java.util.Scanner;
/*
На вход подается баланс счета в банке – n. Рассчитайте дневной бюджет на 30
дней.
*/
public class Solution08 {
    public static final byte MONTH_LEN = 30;
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int count = scanner.nextInt();
        scanner.close();

        System.out.println((double) count / MONTH_LEN);
    }
}
