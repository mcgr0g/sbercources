package hw1p1;

import java.util.Scanner;

/*
На вход подается двузначное число n. Выведите число, полученное
перестановкой цифр в исходном числе n. Если после перестановки получается
ведущий 0, его также надо вывести.
*/
public class Solution07 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        byte count = scanner.nextByte();
        scanner.close();
        System.out.println( (count % 10) + "" + (count / 10));
    }
}
