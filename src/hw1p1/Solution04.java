package hw1p1;

import java.util.Scanner;

/*
На вход подается количество секунд, прошедших с начала текущего дня – count.
Выведите в консоль текущее время в формате: часы и минуты.
*/
public class Solution04 {
    public static void main(String[] args) {
        final int SECONDS_PER_MINUTE = 60,
                MINUTES_PER_HOUR = 60,
                HOURS_PER_DAY = 24;

        int currentSecond,
                totalMinutes, currentMinute,
                totalHours, currentHour;

        Scanner scanner = new Scanner(System.in);
        int count = scanner.nextInt();
        scanner.close();

        currentSecond = count % SECONDS_PER_MINUTE;

        totalMinutes = count / SECONDS_PER_MINUTE;
        currentMinute = totalMinutes % MINUTES_PER_HOUR;

        totalHours = totalMinutes / MINUTES_PER_HOUR;
        currentHour = totalHours % HOURS_PER_DAY;

        System.out.println(totalHours + " " + currentMinute);
    }
}
