package hw1p1;

import java.util.Scanner;

/*
Переведите дюймы в сантиметры (1 дюйм = 2,54 сантиметров). На вход
подается количество дюймов, выведите количество сантиметров.
*/
public class Solution05 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        short count = scanner.nextShort();
        scanner.close();
        
        final double NUMERATOR = 2.54;
        System.out.println(count * NUMERATOR);
    }
}
